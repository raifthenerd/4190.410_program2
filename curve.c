/* -*- mode: c -*- */
#include "curve.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

enum XY { X = 0, Y };

#define SET_VECTOR2(V, V1, V2)          do { (V)[X] = (V1); (V)[Y] = (V2); } while (0)
#define COPY_PT(DST, SRC)               do { (DST)[X] = SRC[X]; (DST)[Y] = SRC[Y]; } while (0)
#define VECTOR2_X_SCALA_ADD(O, V, S)    do { O[X] += (S) * (V)[X]; O[Y] += (S) * (V)[Y]; } while (0)

#ifdef DEBUG
void PRINT_CTRLPTS(CubicBezierCurve* crv) {
    int i;
    printf("curve %p\n[\n", crv);
    for (i=0; i<4; ++i)
        printf("[%f, %f]\n", crv->control_pts[i][X], crv->control_pts[i][Y]);
    printf("]\n");
}
#endif

void evaluate(const CubicBezierCurve *curve, const REAL t, Point value)
{
    const REAL t_inv = 1.0f - t;
    const REAL t_inv_sq = t_inv * t_inv;
    const REAL t_sq = t * t;
    const REAL b0 = t_inv_sq * t_inv;
    const REAL b1 = 3 * t_inv_sq * t;
    const REAL b2 = 3 * t_inv * t_sq;
    const REAL b3 = t_sq * t;
    SET_VECTOR2(value, 0, 0);
    VECTOR2_X_SCALA_ADD(value, curve->control_pts[0], b0);
    VECTOR2_X_SCALA_ADD(value, curve->control_pts[1], b1);
    VECTOR2_X_SCALA_ADD(value, curve->control_pts[2], b2);
    VECTOR2_X_SCALA_ADD(value, curve->control_pts[3], b3);
}

void calc_inflection_pts(CubicBezierCurve *curve)
{
    REAL a[3],b[3],c[3];
    REAL* tmp = curve->inflection_pts;
    int i, n = 0;
    *tmp = 0.0;
    // x'(t)/3 = 0 and y'(t)/3 = 0
    for (i=0; i<2; ++i) {
        a[i] = curve->control_pts[3][i] -
            3.0f * curve->control_pts[2][i] +
            3.0f * curve->control_pts[1][i] -
            curve->control_pts[0][i];
        b[i] = 2.0f * curve->control_pts[2][i] -
            4.0f * curve->control_pts[1][i] +
            2.0f * curve->control_pts[0][i];
        c[i] = curve->control_pts[1][i] -
            curve->control_pts[0][i];
    }
    // (x''(t)*y'(t)-x'(t)*y''(t))/18 = 0
    a[2] = a[1]*b[0] - a[0]*b[1];
    b[2] = 2*(a[1]*c[0] - a[0]*c[1]);
    c[2] = b[1]*c[0] - b[0]*c[1];
    for (i=0; i<3; ++i) {
        n += quadratic_solver(a[i], b[i], c[i], tmp+n+1);
    }
    *(tmp+n+1) = 1.0;
    qsort(tmp+1, n, sizeof(REAL), cmpreal);
    curve->n_inflection_pts = n;
}

int quadratic_solver(const REAL a, const REAL b, const REAL c, REAL* root)
/* solve ax^2+bx+c=0 and save to given array
 * return number of roots
 */
{
    REAL* pt = root;
    if (a == 0.0) {
        if (b != 0.0) {
            *pt = -c/b;
            if (*pt > 0.0 && *pt < 1.0) {
                return 1;
            }
        }
        return 0;
    } else {
        REAL det = b*b - 4*a*c;
        if (det < 0.0) {
            return 0;
        } else if (det == 0.0) {
            *pt = -b/a/2;
            if (*pt > 0.0 && *pt < 1.0) {
                return 1;
            }
            return 0;
        } else {
            int i = 0;
            det = sqrt(det)/a/2;
            *pt = -det-b/a/2;
            if (*pt > 0.0 && *pt < 1.0) {
                i++;
                pt++;
            }
            *pt = det-b/a/2;
            if (*pt > 0.0 && *pt < 1.0) {
                i++;
            }
            return i;
        }
    }
}

int cmpreal(const void* a, const void* b)
{
    if ( *(REAL*)a <  *(REAL*)b ) return -1;
    if ( *(REAL*)a >  *(REAL*)b ) return 1;
    return 0;
}

void draw_bounding_boxes(const CubicBezierCurve* curve,
                         const REAL t0, const REAL t1,
                         REAL (*box_type)
                         (const CubicBezierCurve*, const REAL, const REAL))
{
    REAL t2 = box_type(curve, t0, t1);
    if (t2 - t0 > 0.01) {
        draw_bounding_boxes(curve, t0, t2, box_type);
    }
    if (t1 - t2 > 0.01) {
        draw_bounding_boxes(curve, t2, t1, box_type);
    }
}
