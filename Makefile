CC = gcc
CFLAGS = -Wall -g
INCLUDES =
LFLAGS =
LIBS = -lGL -lGLU -lglut -lm

MAIN = bezier.out
SRCS = main.c curve.c
OBJS = $(SRCS:.c=.o)

.PHONY: clean
all: $(MAIN)
$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
clean:
	$(RM) *.o $(MAIN)
