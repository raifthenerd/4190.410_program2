/* -*- mode: c -*- */
#include <GL/glut.h>
#include "curve.h"

/* global */
CubicBezierCurve curve[2];
GLsizei width = 640, height = 480;
REAL (*box_type) (const CubicBezierCurve*, const REAL, const REAL);

/* various implementation of bounding boxes */
REAL aabb(const CubicBezierCurve* curve, const REAL t0, const REAL t1)
{
    Point pt0, pt1;
    evaluate(curve, t0, pt0);
    evaluate(curve, t1, pt1);
    glBegin(GL_LINE_LOOP);
    glVertex2f(pt0[0], pt0[1]);
    glVertex2f(pt1[0], pt0[1]);
    glVertex2f(pt1[0], pt1[1]);
    glVertex2f(pt0[0], pt1[1]);
    glEnd();
    return (t0 + t1) / 2;
}
REAL obb(const CubicBezierCurve* curve, const REAL t0, const REAL t1)
{
    int i,n;
    REAL a[3],b[3],c[3],root[2],t2;
    Point pt0, pt1, pt2;
    evaluate(curve, t0, pt0);
    evaluate(curve, t1, pt1);
    for (i=0; i<2; ++i) {
        a[i] = curve->control_pts[3][i] -
            3.0f * curve->control_pts[2][i] +
            3.0f * curve->control_pts[1][i] -
            curve->control_pts[0][i];
        b[i] = 2.0f * curve->control_pts[2][i] -
            4.0f * curve->control_pts[1][i] +
            2.0f * curve->control_pts[0][i];
        c[i] = curve->control_pts[1][i] -
            curve->control_pts[0][i];
    }
    a[2] = (pt1[0]-pt0[0]) * a[1] - (pt1[1]-pt0[1]) * a[0];
    b[2] = (pt1[0]-pt0[0]) * b[1] - (pt1[1]-pt0[1]) * b[0];
    c[2] = (pt1[0]-pt0[0]) * c[1] - (pt1[1]-pt0[1]) * c[0];
    n = quadratic_solver(a[2], b[2], c[2], (REAL*)root);
    for (i=0; i<n; ++i) {
        if (root[i] > t0 && root[i] < t1) {
            t2 = root[i];
        }
    }
    evaluate(curve, t2, pt2);
    REAL x, y, z, w, k;
    REAL dx, dy;
    x = pt1[0] - pt0[0];
    y = pt1[1] - pt0[1];
    z = pt2[0] - pt0[0];
    w = pt2[1] - pt0[1];
    k = (w*x-z*y)/(x*x+y*y);
    dx = -y*k;
    dy = x*k;
    glBegin(GL_LINE_LOOP);
    glVertex2f(pt0[0], pt0[1]);
    glVertex2f(pt1[0], pt1[1]);
    glVertex2f(pt1[0]+dx, pt1[1]+dy);
    glVertex2f(pt0[0]+dx, pt0[1]+dy);
    glEnd();
    return t2;
}

/* intersection point between two curve segments */
int intersect(const CubicBezierCurve* curve1, const CubicBezierCurve* curve2,
              REAL t11, REAL t12, REAL t21, REAL t22, Point pt[])
{
    int n;
    REAL tmp1, tmp2;
    Point pt11, pt12, pt21, pt22;
    REAL box_x[4], box_y[4];
    evaluate(curve1, t11, pt11);
    evaluate(curve1, t12, pt12);
    evaluate(curve2, t21, pt21);
    evaluate(curve2, t22, pt22);
    tmp1 = aabb(curve1, t11, t12);
    aabb(curve2, t21, t22);
    box_x[0] = pt11[0];
    box_x[1] = pt12[0];
    qsort(box_x, 2, sizeof(REAL), cmpreal);
    box_y[0] = pt11[1];
    box_y[1] = pt12[1];
    qsort(box_y, 2, sizeof(REAL), cmpreal);
    box_x[2] = pt21[0];
    box_x[3] = pt22[0];
    qsort(box_x+2, 2, sizeof(REAL), cmpreal);
    box_y[2] = pt21[1];
    box_y[3] = pt22[1];
    qsort(box_y+2, 2, sizeof(REAL), cmpreal);
    if (0 < box_x[3] - box_x[0] && box_x[1] - box_x[2] > 0 &&
        0 < box_y[3] - box_y[0] && box_y[1] - box_y[2] > 0)
    {
        qsort(box_x, 4, sizeof(REAL), cmpreal);
        qsort(box_y, 4, sizeof(REAL), cmpreal);
        if (t22 - t21 > 0.002) {
            n = intersect(curve2, curve1, t21, t22, t11, tmp1, pt);
            n += intersect(curve2, curve1, t21, t22, tmp1, t12, pt+n);
            return n;
        } else {
            (*pt)[0] = (box_x[1] + box_x[2]) / 2;
            (*pt)[1] = (box_y[1] + box_y[2]) / 2;
            return 1;
        }
    }
    return 0;
}

int hit_index(CubicBezierCurve *curve, int x, int y)
{
    int i;
    for (i=0; i<4; ++i) {
        REAL tx = curve->control_pts[i][0] - x;
        REAL ty = curve->control_pts[i][1] - y;
        if ((tx * tx + ty * ty) < 30)
            return i;
    }
    return -1;
}

void init()
{
    SET_PT2(curve[0].control_pts[0], 50, 100);
    SET_PT2(curve[0].control_pts[1], 200, 300);
    SET_PT2(curve[0].control_pts[2], 400, 300);
    SET_PT2(curve[0].control_pts[3], 550, 100);
    SET_PT2(curve[1].control_pts[0], 150, 100);
    SET_PT2(curve[1].control_pts[1], 200, 400);
    SET_PT2(curve[1].control_pts[2], 400, 400);
    SET_PT2(curve[1].control_pts[3], 450, 100);

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0, width, 0, height);

    box_type = aabb;
}

void reshape_callback(GLint nw, GLint nh)
{
    width = nw;
    height = nh;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, width, 0, height);
}

void display_callback()
{
#define RES 100
    int i,j,k;
    glClear(GL_COLOR_BUFFER_BIT);
    for (i=0; i<2; ++i) {
        glColor3ub(0, 0, 0);
        /* curve */
        glBegin(GL_LINE_STRIP);
        for (j=0; j<=RES; ++j) {
            Point pt;
            const REAL t = (REAL)j / (REAL)RES;
            evaluate(curve+i, t, pt);
            glVertex2f(pt[0], pt[1]);
        }
        glEnd();

        /* control mesh */
        glColor3ub(255, 0, 0);
        glBegin(GL_LINE_STRIP);
        for (j=0; j<4; ++j) {
            REAL *pt = curve[i].control_pts[j];
            glVertex2f(pt[0], pt[1]);
        }
        glEnd();

        /*
         * assignment 2, part i:
         *
         * compute all x and y-extreme points as well as all inflection points
         * of C(t), 0 <= t <= 1, by solving the following three quadratic
         * equations.
         *
         */
        calc_inflection_pts(curve+i);
        /* glColor3ub(0, 255, 0); */
        /* glPointSize(7.5); */
        /* glBegin(GL_POINTS); */
        /* for (j=1; j<=curve[i].n_inflection_pts; ++j) { */
        /*     Point pt; */
        /*     evaluate(curve+i, curve[i].inflection_pts[j], pt); */
        /*     glVertex2f(pt[0], pt[1]); */
        /* } */
        /* glEnd(); */

        /*
         * assignment 2, part ii:
         *
         * subdivide the cubic bezier curve C(t) into monotone convex/concave
         * subsegments C_i(u_i), 0 <= u_i <= 1, i = 1,...,N, by subdividing at
         * x and y-extreme points as well as at inflection points. for each
         * segment C_i(u_i), construct an AABB tree, an OBB tree, and an arc
         * tree.
         *
         */
        /* glColor3ub(0, 255, 0); */
        /* for (j = 0; j <= curve[i].n_inflection_pts; ++j) { */
        /*     draw_bounding_boxes(curve+i, curve[i].inflection_pts[j], */
        /*                         curve[i].inflection_pts[j+1], box_type); */
        /* } */

        /* control pts */
        glColor3ub(0, 0, 255);
        glPointSize(10.0);
        glBegin(GL_POINTS);
        for (j=0; j<4; ++j) {
            REAL *pt = curve[i].control_pts[j];
            glVertex2f(pt[0], pt[1]);
        }
        glEnd();
    }
    /*
     * assignment 2, part iii:
     *
     * implement an algorithm for computing the intersection points between the
     * two cubic Bezier curves using their AABB trees. Moreover, implement an
     * algorithm that computes the self-intersection points for each curve.
     * Display the bounding volumes that have been used in the search for the
     * intersection points and the self-intersection points.
     *
     */
    Point foo[1024], bar[1024];
    int n_foo = 0, n_bar = 0;
    glPointSize(7.5);
    glColor3ub(0, 127, 0);
    for(i=0; i<=curve[0].n_inflection_pts; ++i) {
        for(j=0; j<=curve[1].n_inflection_pts; ++j) {
            n_foo +=
                intersect(curve, curve+1,
                          curve[0].inflection_pts[i], curve[0].inflection_pts[i+1],
                          curve[1].inflection_pts[j], curve[1].inflection_pts[j+1],
                          foo+n_foo);
        }
    }
    for(i=0; i<2; ++i) {
        for(j=0; j<curve[i].n_inflection_pts; ++j) {
            for(k=j+1; k<=curve[i].n_inflection_pts; ++k) {
                n_bar +=
                    intersect(curve+i, curve+i,
                              curve[i].inflection_pts[j],
                              curve[i].inflection_pts[j+1],
                              curve[i].inflection_pts[k],
                              curve[i].inflection_pts[k+1],
                              bar+n_bar);
            }
        }
    }
    glColor3ub(255, 0, 0);
    for (i=0; i<n_foo; ++i) {
        glBegin(GL_POINTS);
        glVertex2f(foo[i][0], foo[i][1]);
        glEnd();
    }
    glColor3ub(255, 0, 255);
    for (i=0; i<n_bar; ++i) {
        glBegin(GL_POINTS);
        glVertex2f(bar[i][0], bar[i][1]);
        glEnd();
    }

    glutSwapBuffers();
}

void mouse_callback(GLint button, GLint action, GLint x, GLint y)
{
    int i;
    for (i=0; i<2; ++i) {
        if (GLUT_LEFT_BUTTON == button) {
            switch (action) {
            case GLUT_DOWN:
                curve[i].edit_ctrlpts = hit_index(curve+i, x, height - y);
                break;

            case GLUT_UP:
                curve[i].edit_ctrlpts = -1;
                break;
            }
        }
    }
    glutSwapBuffers();
}

void mouse_move_callback(GLint x, GLint y)
{
    int i;
    for (i=0; i<2; ++i) {
        if (curve[i].edit_ctrlpts != -1) {
            curve[i].control_pts[curve[i].edit_ctrlpts][0] = x;
            curve[i].control_pts[curve[i].edit_ctrlpts][1] = height - y;
        }
        glutPostRedisplay();
    }
}

void keyboard_callback(unsigned char key, GLint x, GLint y)
{
    if (box_type == obb) {
        box_type = aabb;
    } else if (box_type == aabb) {
        box_type = obb;
    }
    glutPostRedisplay();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutCreateWindow("Beizer Editor");

    init();
    glutReshapeFunc(reshape_callback);
    glutMouseFunc(mouse_callback);
    glutMotionFunc(mouse_move_callback);
    glutDisplayFunc(display_callback);
    // glutKeyboardFunc(keyboard_callback);
    glutMainLoop();
    return 0;
}
